import React,{useState,useContext} from 'react'
import {CharStats} from '../../context'

import './styles.css'
const Form = (props) =>{
    let data = useContext(CharStats)
    const [nameState, setName] = useState('')
    const [strState, setStr] = useState('')
    const [hpState, setHp] = useState('')
    const [speedState, setSpeed] = useState('')

    if(localStorage.getItem('user') !== null){
        data.isSend = true  
    }
    const handleName = (e) =>{
        setName(e.target.value)
    }
    const handleStr = (e) =>{
        setStr(e.target.value)
    }
    const handleHp = (e) =>{
        setHp(e.target.value)
    }
    const handleSpeed = (e) =>{
        setSpeed(e.target.value)
    }
    const handleSubmit = e =>{
        // //e.preventDefault()
        alert('Ready To Play' + nameState)
        let user = {
            name:nameState,
            str:strState,
            hp:hpState,
            speed:speedState
        }
            user = JSON.stringify(user)
            localStorage.setItem('user',user)
    }
  
        return(           
            <div className="container">
            <div className="form-wrapper">
            <h1>Create Character</h1>
            <form action="" onSubmit ={(e)=>{handleSubmit(e)}}>
                <label htmlFor="">Name : </label>
                <input type="text" name="name" value={nameState} onChange={(e)=>{handleName(e)}}/>
                <label htmlFor="">Strength : </label>
                <input type="text" name="str" value={strState} onChange={(e)=>{handleStr(e)}}/>
                <label htmlFor="">Hp : </label>
                <input type="text" name="hp" value={hpState} onChange={(e)=>{handleHp(e)}}/>
                <label htmlFor="">Speed : </label>
                <input type="text" name="spped" value={speedState} onChange={(e)=>{handleSpeed(e)}}/>
                <input type="submit" name="send" />
            </form>
            </div>   
        
            </div>
        )
 
}


export default Form
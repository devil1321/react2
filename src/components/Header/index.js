import React,{Component} from 'react'
const Header = ({name}) =>{

    return(
        <div>
            <h1>To jest kurs react.js czyli EasyCode witam {name}</h1>
        </div>
    )
}

// class Header extends Component{
//     render(){
//         return(
//             <div>
//                 <h1>To jest kurs react.js czyli EasyCode {this.props.name}</h1>
//             </div>
//         )
//     }
// }


export default Header
import {useContext} from 'react'
import {CharStats} from '../../context'
import './styles.css'

const Opponent = (props) =>{    

  const {name,str,hp,speed,addStr} = useContext(CharStats)
    return(
        <div className="col-6">
            <h1 className="user__level">{props.poziom} {name}</h1>
            <p className="user__name">Name : {name}</p>
            <p className="user__str ">Str : {str}</p>
            <p className="user__hp">Hp : {hp}</p>
            <p className="user__speed">Speed: {speed}</p>
        </div>
    )
}
export default Opponent
import './App.css';
import Homepage from './pages/homepage'

function App() {
  
  return (
    <div className="App">
      <Homepage name='Easy Code'/>
    </div>
  );
}

export default App;

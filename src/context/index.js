import React,{createContext,useState,useEffect} from 'react'

export const CharStats = createContext({
    name:'Starter',
    str:0,
    hp:0,
    speed:0,
    addStr:item => {},
    isSend:false
  
})

export const UserStatsProvider = ({children}) =>{
        let data = localStorage.getItem('user') 
        data = JSON.parse(data)
        const name = data.name || null
        const [ str, addStr ] = useState(data.str || null)
        const hp = data.hp || null;
        const speed = data.speed || null;
    return(
        <CharStats.Provider value={{
            name,
            str,
            hp,
            speed,
            addStr
        }}>
            {children}
        </CharStats.Provider>
    )
}
export const OpponentStatsProvider = ({children}) =>{
    const name = 'Punisher';
    const str = 10;
    const hp = 12;
    return(
        <CharStats.Provider value={{
            name,
            str,
            hp,
        }}>
            {children}
        </CharStats.Provider>
    )
}
import React,{useContext,useState} from 'react'
import Header from '../../components/Header'
import User from '../../components/user'
import Opponent from '../../components/Opponent'
import Klasa from '../../components/Klasa'
import Form from '../../components/Form'
import {UserStatsProvider, OpponentStatsProvider} from '../../context'
import {CharStats} from '../../context'
import './styles.css'

const Homepage = (props) => {

  const data = useContext(CharStats)
  if(localStorage.getItem('user') !== null){
    data.isSend = true  
  } 
  const handleAddUser = () =>{
    localStorage.clear()
    window.location.reload()
  }
  if(data.isSend){
    return (
      <React.Fragment>
        <div className="App-header">
        <UserStatsProvider >
          <User poziom = "10" /> 
        </UserStatsProvider>
        <OpponentStatsProvider>
              <Opponent poziom = "10" />
        </OpponentStatsProvider>
   
        </div>
        <button className="btn" onClick = {()=>handleAddUser()}>Add User</button>
      </React.Fragment>
      )
  } else  {
    return (
      <div className="form-container">
        <Form />
      </div>
    )
  }
}

export default Homepage;
